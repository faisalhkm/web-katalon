<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn Login</name>
   <tag></tag>
   <elementGuidId>e4d0a84d-3308-4ba1-a000-ac44ec5df820</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[text()='Login']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[text()='Login']</value>
      <webElementGuid>63a3eb84-763d-4d97-a117-9aca0de9121d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
