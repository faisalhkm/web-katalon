<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn Customer</name>
   <tag></tag>
   <elementGuidId>c3b3b57d-4a6f-4e67-aa93-eb6ab9ef01d1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@href='#/customer']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[@href='#/customer']</value>
      <webElementGuid>50d6e87e-2b11-4f6e-a3af-ec569ab8a8f6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
