<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Ipt Search</name>
   <tag></tag>
   <elementGuidId>1c60fb38-d4e1-4880-b391-a90abbfccbf9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@placeholder='Search Transaction ID']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@placeholder='Search Transaction ID']</value>
      <webElementGuid>27e90154-4f75-47a6-9a9f-b9b872f8e845</webElementGuid>
   </webElementProperties>
</WebElementEntity>
