@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @tag1
  Scenario Outline: Title of your scenario outline
    Given I open dashboard travy
    When I redirect to login page
    And I input valid <email> and <password>
    And I click login button
    Then I see dashboard homepage
    
    Examples: 
      | email  | password |
      | admintravy@gmail.com | adminTravy$123 |