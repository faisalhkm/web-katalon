<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn Logout</name>
   <tag></tag>
   <elementGuidId>d8bf70c5-4e77-4998-9697-f026b6dd0c3e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[text()='Logout ']</value>
      <webElementGuid>ae75238b-32cf-4155-a477-3b89c3567411</webElementGuid>
   </webElementProperties>
</WebElementEntity>
