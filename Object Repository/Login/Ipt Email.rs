<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Ipt Email</name>
   <tag></tag>
   <elementGuidId>34e05e94-17eb-4a8c-b072-2adfd1bb7e6f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type='email']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type='email']</value>
      <webElementGuid>c961a15e-31d9-41bc-841d-71b514b3fcc1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
